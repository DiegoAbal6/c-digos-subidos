/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;
import javax.swing.JOptionPane;

/**
 *
 * @author Diego
 * @author Alan
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Calculadora funciones = new Calculadora();
        float n1, n2;
        
        do
        {
            n1 = Float.parseFloat(JOptionPane.showInputDialog("Introduzca Primer Numero"));
            n2 = Float.parseFloat(JOptionPane.showInputDialog("Introduzca Segundo Numero"));
            
            switch (Integer.parseInt(JOptionPane.showInputDialog("Que Desea Hacer. \n1- Suma\n2- Resta\n3- Multiplicación\n4- División")))
            {
                default:
                    JOptionPane.showMessageDialog(null, "OPCION INCORRECTA", "oh oh ERROR!!!!!", JOptionPane.INFORMATION_MESSAGE);
                    break;
                case 1:
                    funciones.mostrarSuma(n1, n2);
                    break;
                case 2: 
                    funciones.mostrarResta(n1, n2);
                    break;
                case 3:
                    funciones.mostrarMultiplicacion(n1, n2);
                    break;
                case 4:
                    funciones.mostrarDivision(n1, n2);
                    break;
            }        
        }while (Integer.parseInt(JOptionPane.showInputDialog("¿Desea Ejecutar De Nuevo? \n1- SI \n2- NO"))==1);
    }
    
    //METODOS 
    public float suma(float n1, float n2) {
        return n1 + n2;
    }

    public void mostrarSuma(float n1, float n2){
        float resultado=suma(n1,n2);
        JOptionPane.showMessageDialog(null, resultado, "Resultado De La Suma", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public float resta (float n1, float n2){
        return n1 - n2;
    }
    public void mostrarResta(float n1, float n2){
        float resultado;
        int op=0;
        if (n1 < n2) op = Integer.parseInt(JOptionPane.showInputDialog("Primer Numero Menor Que Desea Hacer?\n1- Invertir Numeros\n2- Continuar Con La Operacion"));
        if (op==1)
        {
            resultado = resta(n2,n1);            
        }
        else
        {
            resultado = resta(n1,n2);
        }
        JOptionPane.showMessageDialog(null, resultado, "Resultado De La Resta", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public float multiplicacion(float n1, float n2){
        return n1*n2;        
    }
    
    public void mostrarMultiplicacion(float n1, float n2){        
        float resultado;
        resultado = multiplicacion(n1,n2);
        JOptionPane.showMessageDialog(null, resultado, "Resultado de multiplicación" , JOptionPane.INFORMATION_MESSAGE);        
    }
    
    public float division (float n1, float n2){
        return n1 / n2;
    }
    public void mostrarDivision(float n1, float n2){
        float resultado;
        int op=0;
        if (n1 < n2) op = Integer.parseInt(JOptionPane.showInputDialog("Primer Numero Menor Que Desea Hacer?\n1- Invertir Numeros\n2- Continuar Con La Operacion"));
        if (op==1)
        {
            resultado = resta(n2,n1);            
        }
        else
        {
            resultado = resta(n1,n2);
        }
        JOptionPane.showMessageDialog(null, resultado, "Resultado De La División", JOptionPane.INFORMATION_MESSAGE);
    }
}
